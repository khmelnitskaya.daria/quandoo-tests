from appium.webdriver.common.touch_action import TouchAction

from framework.appium_driver_manager.appium_driver_manager import AppiumDriverManager


class Base:
    def __init__(self, platform):
        self.manager = AppiumDriverManager(platform)
        self.driver = self.manager.driver()
        self.app = self.driver.desired_capabilities.get('app')
        self.platform = self.driver.desired_capabilities.get('platformName')
        self.device_name = self.driver.desired_capabilities.get('deviceName')


    def scroll(self, element_from, element_to):
        action = TouchAction(self.driver)
        el1 = self.driver.find_element_by_accessibility_id(element_from)
        el2 = self.driver.find_element_by_accessibility_id(element_to)
        action.press(el1).move_to(el2).release().perform()
