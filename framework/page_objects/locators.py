class Locators:
    def __init__(self, platform):
        self.mobile = platform

    @property
    def burger(self):
        if self.mobile == "android":
            return ('id', 'burger', 'Burger button in search field')
        else:
            return ('id', 'burger', 'Burger button in search field')

    @property
    def profile_tab(self):
        if self.mobile == "android":
            return ('id', 'profile_tab', 'Button Profile on tabbar')
        else:
            return ('id', 'profile_tab', 'Button Profile on tabbar')

    @property
    def discovery_tab(self):
        if self.mobile == "android":
            return ('id', 'discovery_tab', 'Button Discovery on tabbar')
        else:
            return ('id', 'discovery_tab', 'Button Discovery on tabbar')

    @property
    def sign_up_btn(self):
        if self.mobile == "android":
            return ('id', 'sign_up_btn', 'Button Log in or Sign Up')
        else:
            return ('id', 'sign_up_btn', 'Button Log in or Sign Up')

    @property
    def log_in_or_sign_up(self):
        if self.mobile == "android":
            return ('id', 'log_in_or_sign_up', 'Button Log in or Sign Up in burger menu')
        else:
            return ('id', 'log_in_or_sign_up', 'Button Log in or Sign Up in burger menu')

    @property
    def sign_up_with_email_btn(self):
        if self.mobile == "android":
            return ('id', 'sign_up_with_email_btn', 'Button Continue with email address')
        else:
            return ('id', 'sign_up_with_email_btn', 'Button Continue with email address')

    @property
    def sign_up_now_btn(self):
        if self.mobile == "android":
            return ('id', 'sign_up_now_btn', 'Button Sign up now')
        else:
            return ('id', 'sign_up_now_btn', 'Button Sign up now')

    @property
    def sign_up_first_name(self):
        if self.mobile == "android":
            return ('id', 'sign_up_first_name', 'First name on sign up page')
        else:
            return ('id', 'sign_up_first_name', 'First name on sign up page')

    @property
    def sign_up_last_name(self):
        if self.mobile == "android":
            return ('id', 'sign_up_last_name', 'Last name on sign up page')
        else:
            return ('id', 'sign_up_last_name', 'Last name on sign up page')

    @property
    def sign_up_email(self):
        if self.mobile == "android":
            return ('id', 'sign_up_email', 'Email on sign up page')
        else:
            return ('id', 'sign_up_email', 'Email on sign up page')

    @property
    def sign_up_password(self):
        if self.mobile == "android":
            return ('id', 'sign_up_password', 'Password on sign up page')
        else:
            return ('id', 'sign_up_password', 'Password on sign up page')

    @property
    def sign_up_confirm_password(self):
        if self.mobile == "android":
            return ('id', 'sign_up_confirm_password', 'Confirm Password on sign up page')
        else:
            return ('id', 'sign_up_confirm_password', 'Confirm Password on sign up page')

    @property
    def checkbox_on_sign_up_page(self):
        if self.mobile == "android":
            return ('id', 'checkbox_on_sign_up_page', 'Checkbox on sign up page')
        else:
            return ('id', 'checkbox_on_sign_up_page', 'Checkbox on sign up page')

    @property
    def create_account(self):
        if self.mobile == "android":
            return ('id', 'create_account', 'Create account btn on sign up page')
        else:
            return ('id', 'create_account', 'Create account btn on sign up page')

    @property
    def name_of_account(self):
        if self.mobile == "android":
            return ('id', 'name_of_account', 'Create account btn on sign up page')
        else:
            return ('id', 'name_of_account', 'Create account btn on sign up page')

    @property
    def search_field(self):
        if self.mobile == "android":
            return ('id', 'search_field', 'Search field on discovery page')
        else:
            return ('id', 'search_field', 'Search field on discovery page')

    @property
    def city_field(self):
        if self.mobile == "android":
            return ('id', 'city_field', 'City field on search page')
        else:
            return ('id', 'city_field', 'City field on search page')

    @property
    def see_all_restaurants_btn(self):
        if self.mobile == "android":
            return ('id', 'see_all_restaurants_btn', 'See all restaurants button on search page')
        else:
            return ('id', 'see_all_restaurants_btn', 'See all restaurants button on search page')

    @property
    def open_now_btn(self):
        if self.mobile == "android":
            return ('id', 'open_now_btn', 'Open now button on list page')
        else:
            return ('id', 'open_now_btn', 'Open now button on list page')

    @property
    def additional_filters_btn(self):
        if self.mobile == "android":
            return ('id', 'additional_filters_btn', 'Additional filters button')
        else:
            return ('id', 'additional_filters_btn', 'Additional filters button')

    @property
    def table_for(self):
        if self.mobile == "android":
            return ('id', 'table_for', 'Number of people on reservation details page')
        else:
            return ('id', 'table_for', 'Number of people on reservation details page')

    @property
    def date(self):
        if self.mobile == "android":
            return ('id', 'date', 'Date on reservation details page')
        else:
            return ('id', 'date', 'Date on reservation details page')

    @property
    def apply_btn(self):
        if self.mobile == "android":
            return ('id', 'apply_btn', 'Apply button on reservation details page')
        else:
            return ('id', 'apply_btn', 'Apply button on reservation details page')

    @property
    def restaurant_in_list(self):
        if self.mobile == "android":
            return ('id', 'restaurant_in_list', 'Restaurant in list page')
        else:
            return ('id', 'restaurant_in_list', 'Restaurant in list page')

    @property
    def menu_section(self):
        if self.mobile == "android":
            return ('id', 'menu_section', 'Menu section on restaurant page')
        else:
            return ('id', 'menu_section', 'Menu section on restaurant page')

    @property
    def review_section(self):
        if self.mobile == "android":
            return ('id', 'review_section', 'Review section on restaurant page')
        else:
            return ('id', 'review_section', 'Review section on restaurant page')
