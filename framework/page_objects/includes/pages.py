from actions.search_action import SearchActions
from actions.sign_up_action import AccountActions
from framework.page_objects.locators import Locators
from framework.wrapper import Base


class Pages(Base):
    def __init__(self):
        super().__init__()

    @property
    def locators(self):
        return Locators()

    @property
    def account_actions(self):
        return AccountActions()

    @property
    def search_actions(self):
        return SearchActions()
