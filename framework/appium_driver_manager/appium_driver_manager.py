from appium import webdriver

android_capabilities = {
    'automationName': 'UiAutomator2',
    'platformName': 'Android',
    # 'app': 'Quandoo',
    'appPackage': 'Quandoo',
    'appActivity': 'Quandoo.PreStartActivity'
}

ios_capabilities = {
    'automationName': 'XCUITest',
    'platformName': 'iOS',
    'platformVersion': '14.5',
    'app': 'Quandoo',
    'appPackage': 'Quandoo'
}


class AppiumDriverManager():
    def __init__(self, platform: str):
        self.caps = self.capabilities(platform)
        self.appium = webdriver.Remote('http://0.0.0.0:4723/wd/hub', self.caps)

    def driver(self):
        if self.appium:
            return self.appium
        raise ReferenceError('Please initialize appium: AppiumDriverManager().init()')

    def quit(self):
        if self.appium:
            self.appium.quit()

    def capabilities(self, platform):
        capabilities = {}
        if platform == 'android':
            capabilities = android_capabilities
        if platform == 'ios':
            capabilities = ios_capabilities
        return capabilities
