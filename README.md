I selected pytest+AppiumWebDriver. 
Appium is suitable for both platforms - android and iOS. And Appium's actions are really similar to those of a real user.
I choose pytest because with pytest is easy to write simple test cases.
I also used Autotest Design Patterns - Page Object to put there actions and properties that will be used often.

To make the framework work:
 - change capabilities, change locators.

For improvement I'll:
 - add method, that will wait an element on page. Wait Method good to use after click to get more stable test. 
 - rewrite basic Appium methods in wrapper, to make them more suitable for necessity and make them more readable in tests.
 - separate locators for different pages.
 - add method that will prepare app (wait ending of animation, close some alerts).
 - add allure for visualization test results.
 - remake some precondition with using api (for ex. - sign up). And last one - I'll go to some pages with deeplinks (if this page is not a point of test case).  

