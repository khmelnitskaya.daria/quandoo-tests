import pytest
from appium.webdriver.common.appiumby import AppiumBy
from framework.page_objects.locators import Locators
from actions.search_action import SearchActions
from actions.sign_up_action import AccountActions

from framework.wrapper import Base

@pytest.mark.parametrize('platform', ['android', 'ios'])
def test_apply_filters(platform):
    wrapper = Base(platform)
    locator = Locators(wrapper.platform)
    search_actions = SearchActions(wrapper)
    account_actions = AccountActions(wrapper)
    try:
        # Step 1. Sign up via an email address (on app for Android I haven't seen  tabbar and went to sign up from burger)
        if platform == 'android':
            burger = wrapper.driver.find_element_by_accessibility_id(locator.burger)
            burger.click()
        else:
            profile_tab_bar = wrapper.driver.find_element_by_accessibility_id(locator.profile_tab)
            profile_tab_bar.click()
        log_in_or_sign_up = wrapper.driver.find_element_by_accessibility_id(locator.log_in_or_sign_up)
        log_in_or_sign_up.click()
        account_actions.sign_up()

        # Step 2. Applying filters
        discovery_tab_bar = wrapper.driver.find_element_by_accessibility_id(locator.profile_tab)
        discovery_tab_bar.click()
        search_actions.search_restaurants(cuisine='Italian')
        open_now_btn = wrapper.driver.find_element_by_accessibility_id(locator.open_now_btn)
        open_now_btn.click()
        search_actions.reservation_details(number_of_people=4)

        # Step 3. Go to the restaurant and scroll to review section
        restaurant_in_list = wrapper.driver.find_element(AppiumBy.ACCESSIBILITY_ID, locator.restaurant_in_list)
        restaurant_in_list[1].click()
        wrapper.scroll(element_from=locator.menu_section, element_to=locator.review_section)
    finally:
        wrapper.driver.quit()
