from framework.page_objects.locators import Locators


class AccountActions:

    def __init__(self, wrapper):
        self.wrapper = wrapper
        self.loc = Locators(wrapper.platofrm)

    def sign_up(self):
        """
        Sign up from profile page
        """
        sign_up_with_email_btn = self.wrapper.find_element_by_accessibility_id(self.loc.sign_up_with_email_btn)
        sign_up_with_email_btn.click()
        sign_up_now = self.wrapper.find_element_by_accessibility_id(self.loc.sign_up_now_btn)
        sign_up_now.click()
        fields_on_sign_up = [self.wrapper.find_element_by_accessibility_id(self.loc.sign_up_first_name),
                             self.wrapper.find_element_by_accessibility_id(self.loc.sign_up_last_name),
                             self.wrapper.find_element_by_accessibility_id(self.loc.sign_up_email),
                             self.wrapper.find_element_by_accessibility_id(self.loc.sign_up_password),
                             self.wrapper.find_element_by_accessibility_id(self.loc.sign_up_confirm_password)]
        for field in fields_on_sign_up:
            field.send_keys('Some valid data')
        checkbox = self.wrapper.find_element_by_accessibility_id(self.loc.checkbox_on_sign_up_page)
        checkbox.click()
        create_acc = self.wrapper.find_element_by_accessibility_id(self.loc.create_account)
        create_acc.click()
        assert self.wrapper.find_element_by_accessibility_id(self.loc.name_of_account)
