from appium.webdriver.common.appiumby import AppiumBy
from framework.page_objects.locators import Locators


class SearchActions:

    def __init__(self, wrapper):
        self.wrapper = wrapper
        self.loc = Locators(wrapper.platform)

    def search_restaurants(self, cuisine, city='Berlin'):
        """
        Search restaurants
        :param cuisine: cuisine or name of restaurants to search
        :param city: search city, Berlin by default
        """
        search_field = self.wrapper.find_element_by_accessibility_id(self.loc.search_field)
        search_field.click()
        search_field.send_keys(cuisine)
        city_field = self.wrapper.find_element_by_accessibility_id(self.loc.city_field)
        city_field.send_keys(city)
        see_all_restaurants_btn = self.wrapper.find_element_by_accessibility_id(self.loc.see_all_restaurants_btn)
        see_all_restaurants_btn.click()

    def reservation_details(self, number_of_people):
        """
        Applying filters on the reservation details page
        Date - next day
        Time - any available
        """
        additional_filters_btn = self.wrapper.find_element_by_accessibility_id(self.loc.additional_filters_btn)
        additional_filters_btn.click()
        table_for = self.wrapper.find_element(AppiumBy.ACCESSIBILITY_ID, self.loc.table_for)
        table_for[number_of_people-1].click()
        dates = self.wrapper.find_element(AppiumBy.ACCESSIBILITY_ID, self.loc.date)
        dates[1].click()
        apply_btn = self.wrapper.find_element_by_accessibility_id(self.loc.apply_btn)
        apply_btn.click()





